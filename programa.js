function dibujarPatron(){

    let cuadro = document.getElementById("txtValor");

    let valor = parseInt(cuadro.value);

    let i, k;

    for ( i = 1; i <= valor; i++){

        let cadena = "";

        for (k = valor - i; k > 0; k--){
            cadena = cadena + " ";
        }

        for (k =0; k < i; k++){
            cadena = cadena + "*";
        }
        console.log(cadena);
    }

}

function dibujarRectangulo(){

    let cuadro = document.getElementById("txtValor");
    let valor =  Number(cuadro.value);
    let i, k;
    for(i = 0; i< valor; i++){

        for(i=0;i<valor;i++){
            if(i === 0 || i === valor - 1){
                for(k = 0;k < valor; k++){
                    document.write("*");
                }
            }else {
                for(k = 0; k < valor; k++){
                    if(k ===0 || k === valor - 1){
                        document.write("*");
                    } else{
                        document.write("c");
                    }
                }
            }
            document.write("<br>");

        }
        
        document.write("<br>");

    }

}


function  dibujarPiramide(){

    let cuadro = document.getElementById("txtValor");

    let valor = parseInt(cuadro.value);

    let i, k;

    for ( i = 1; i <= valor; i++){

        let cadena = "";

        for (k = 0; k < i; k++){
            cadena = cadena + "*";
        }

        console.log(cadena);
    }

}
